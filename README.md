# Spesee.org README

## Django v 1.5.8

## Current Installed Apps:
  - django.contrib.auth
  - django.contrib.contenttypes
  - django.contrib.sessions
  - django.contrib.sites
  - django.contrib.comments
  - django.contrib.messages
  - django.contrib.staticfiles
  - django.contrib.admin
  - haystack
  - registration
  - tagging
  - mptt
  - zinnia
  - appconf
  - imagekit

## Python environment requirements:
  - BeautifulSoup
  - enchant
  - markdown
  - Whoosh